
"use strict";


mapboxgl.accessToken = 'pk.eyJ1IjoiamFuY2lhbzEyMyIsImEiOiJja3JwNjd4ZWowZ2l2MzBvMXNsNHNwcWh2In0.5TDZjK7UTIIMGWCjC_0aBg';
const map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v11',
    center: [145.1343136, -37.9110467], // starting position
    zoom: 12
});
map.addControl(new mapboxgl.NavigationControl());
let markers = [];
let buffer = null
let markersObj = [];
map.off('click')
map.on('load', () => {
    addLines([], false)
});

function renderMakers(markersObj) {
    for (let m of markers) {
        let description = `
            ${m.address}
            <button onclick='removeCoords(${m.coords[0]}, ${m.coords[1]})'> Remove</button>`
        let popup = new mapboxgl.Popup()
            .setHTML(description)
        let marker = new mapboxgl.Marker()
            .setLngLat(m.coords)
            .setPopup(popup)
            .addTo(map)
            .togglePopup();
        markersObj.push(marker);
    }
}

function removeCoords(lng, lat) {
    markers = markers.filter((marker) => marker.coords[0] !== lng && marker.coords[1] !== lat)
    if (markersObj.length)
        markersObj.forEach(m => m.remove())
    markersObj = [];
    renderMakers(markersObj)
    let coods = markers.map(m => m.coords);
    addLines(coods)
    renderList()
    let distance = 0;
    for (let i = 1; i < markers.length; i++) {
        distance += calcKm(coods[i], coods[i - 1])
    }

    if (distance != 0) {
        document.getElementById('distance').textContent = distance + ' Km'
        distance = distance.toFixed(2);
    }
    else {
        // document.getElementById('price').value = ''
        document.getElementById('distance').textContent = ''
    }

}


function addLines(coordinates, ref = true) {
    if (ref) {
        map.removeLayer('route');
        map.removeSource('route');
    }

    map.addSource('route', {
        'type': 'geojson',
        'data': {
            'type': 'Feature',
            'properties': {},
            'geometry': {
                'type': 'LineString',
                'coordinates': coordinates
            }
        }
    });
    map.addLayer({
        'id': 'route',
        'type': 'line',
        'source': 'route',
        'layout': {
            'line-join': 'round',
            'line-cap': 'round'
        },
        'paint': {
            'line-color': '#888',
            'line-width': 8
        }
    });
}



const geocoder = new MapboxGeocoder({
    accessToken: mapboxgl.accessToken,
    mapboxgl: mapboxgl
});


geocoder.on('result', function(data) {
    buffer = data

    console.log(buffer)
})

function add() {
    if (!buffer) {
        return
    }
    markers.push({
        coords: buffer.result['center'],
        address: buffer.result['place_name_en-US']
    })
    buffer = null;
    renderMakers(markersObj);


    let coods = markers.map(m => m.coords);
    addLines(coods)

    let distance = 0;
    for (let i = 1; i < markers.length; i++) {
        distance += calcKm(coods[i], coods[i - 1])
    }

    renderList()
    distance = distance.toFixed(2);
    document.getElementById('distance').textContent = distance + ' Km'

}

function renderList() {
    document.getElementById('list').innerHTML = markers.map((m, index) => `<li class="mdl-list__item mdl-list__item--two-line">
                <span class="mdl-list__item-primary-content">
                  <span>${index == 0 ? 'Start' : ('Stop ' + index)}</span>
                  <span class="mdl-list__item-sub-title">${m.address}</span>
                </span>
                <span class="mdl-list__item-secondary-content">
                  <span class="mdl-list__item-secondary-info">
                    <a class="mdl-list__item-secondary-action" href="#" onclick="removeCoords(${m.coords[0]}, ${m.coords[1]})"><button class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-button--colored">
                    <i class="material-icons">delete</i>
                  </button></a></span>
                </span>
              </li>`).join('')
}

function calcKm(c1, c2) {
    // console.log(lat1, lon1, lat2, lon2)
    let [lon1, lat1] = c1;
    let [lon2, lat2] = c2;
    let R = 6371; // km
    const dLat = (lat2 - lat1) * Math.PI / 180;;
    const dLon = (lon2 - lon1) * Math.PI / 180;;
    lat1 = (lat1) * Math.PI / 180;;
    lat2 = (lat2) * Math.PI / 180;;

    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d;
}

function onConfirm() {
    if (markers.length <= 1) {
        alert('Add Least two places');
        return;
    }

    if (!document.getElementById('cartypevalue').value) {
        alert('Select Type');
        return;
    }

    if (!document.getElementById('date').value) {
        alert('Add date');
        return;
    }

    let distance = Number(document.getElementById('distance').textContent.split(" ")[0]);
    let price = 0
    // console.log(document.getElementById('cartypevalue').value)
    if (document.getElementById('cartypevalue').value !== '' && distance !== 0) {
        const rate = CART_TYPE_RATE[document.getElementById('cartypevalue').value]
        price = rate.base + ((distance - 2 > 0) ? distance - 2 : 0) * rate.rate
    }

    price = price.toFixed(2)
    let history = [];
    if (retrieveLSData(BOOKINGS))
        history = retrieveLSData(BOOKINGS)


    if (!confirm(`Estimate Distance ${distance}Km \nEstimate Charge $${price}`)) {
        return;
    }
    
    history.push({
        id: history.length,
        distance,
        price,
        places: markers,
        date: document.getElementById('date').value,
    })
    updateLSData(BOOKINGS, history)

    window.location.href = 'index.html'
}
document.getElementById('geocoder').appendChild(geocoder.onAdd(map));